{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit ImageSVGListDsgn;

{$warn 5023 off : no warning about unused units}
interface

uses
  SVGList, ImageSVGList, ImageSVGListEditor, ImageSVGListEditorForm, 
  ImageSVGListPropEditors, ImageSVGListStrConsts, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('ImageSVGList', @ImageSVGList.Register);
  RegisterUnit('ImageSVGListEditor', @ImageSVGListEditor.Register);
  RegisterUnit('ImageSVGListPropEditors', @ImageSVGListPropEditors.Register);
end;

initialization
  RegisterPackage('ImageSVGListDsgn', @Register);
end.
