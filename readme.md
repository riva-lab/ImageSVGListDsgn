# ImageSVGListDsgn



**ImageSVGListDsgn** is a package for the **[Lazarus Free Pascal RAD IDE](https://www.lazarus-ide.org/)** that implements the design-time component `TImageSVGList` — a list of SVG images instead of regular bitmaps such as `TImageList`. It can generate raster images to target standard `TImageList`. In common cases `TImageSVGList` used to create multi-PPI GUI.

![diagram](docs/diagram.png)



## Dependencies

The package depends on [BGRAControls](https://github.com/bgrabitmap/bgracontrols), which is used for SVG-images handling. You can install **BGRAControls** via `Package > Online Package Manager` or get it from repository and install manually.



## How to install

1. Open **ImageSVGList.lpk** in **Lazarus**.
2. Click `Use > Install`.
3. After **Lazarus** rebuilding, the `TImageSVGList` component will appear on the **Common Controls** tab.
4. Use it and enjoy.
5. See usage example in [examples](examples) directory.



## How not to install

If you want, you don't have to install the package in the **Lazarus IDE**. You can use class `TSVGList` from [SVGList.pas](SVGList.pas) to manage your list of SVG images directly in your source code. But in this case, of course, you won't be able to use the visual editor to manage your list. For external editor see utility [SVG-Icons-List-Maker](https://gitlab.com/riva-lab/SVG-Icons-List-Maker).



## Visual editor

![](docs/editor.png)

If the package is installed double-clicking the **TImageSVGList** component in form under designing opens visual editor. Editor allows you to manage SVG images of list. You can also save your list to or load from `.svglist` file. This file has simple format. It is a text file-container for SVG images: string 0 and following even strings contain name of SVG image, odd strings contain data of SVG images. You can also edit `.svglist` file in separate utility [SVG-Icons-List-Maker](https://gitlab.com/riva-lab/SVG-Icons-List-Maker).



## License

Package releases under [MIT License](license.txt). Copyright (c) 2023 Riva.