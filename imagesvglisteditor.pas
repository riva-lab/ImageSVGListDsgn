{ ImageSVGListEditor
  ------------------------------------------------------------------------------
  Visual editor for TImageSVGList component
  (с) Riva, 2023.10.10
  https://riva-lab.gitlab.io
  https://gitlab.com/riva-lab
  ------------------------------------------------------------------------------

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit ImageSVGListEditor;

{$mode objfpc}{$H+}

interface

uses
  Forms, PropEdits, ComponentEditors, SVGList,
  ImageSVGListStrConsts, ImageSVGList, ImageSVGListEditorForm;

type

  { TImageSVGListEditor
    -------------------
    Implement items in popup menu of TImageSVGList component
    in Object Inspector and Form Designer }

  TImageSVGListEditor = class(TComponentEditor)
  public
    function GetVerbCount: Integer; override;
    function GetVerb(Index: Integer): String; override;
    procedure ExecuteVerb(Index: Integer); override;

    function DoExecute: Boolean;
    function DoClear: Boolean;
  end;

procedure Register;


implementation

procedure Register;
  begin
    RegisterComponentEditor(TImageSVGList, TImageSVGListEditor);
  end;


 { TImageSVGListEditor }

function TImageSVGListEditor.GetVerbCount: Integer;
  begin
    Result := 2;
  end;

function TImageSVGListEditor.GetVerb(Index: Integer): String;
  begin
    case Index of
      0: Result := rsMenuEdit;
      1: Result := rsMenuClear;
      else
        Result  := '';
      end;
  end;

procedure TImageSVGListEditor.ExecuteVerb(Index: Integer);
  var
    Hook:  TPropertyEditorHook;
    modif: Boolean = False;
  begin
    GetHook(Hook);

    case Index of
      0: modif := DoExecute;
      1: modif := DoClear;
      end;

    if Assigned(Hook) and modif then Hook.Modified(Self);
  end;

function TImageSVGListEditor.DoExecute: Boolean;
  var
    editor: TfmImageSVGListEditor;
  begin
    if not (GetComponent is TImageSVGList) then Exit;

    editor := TfmImageSVGListEditor.Create(Application);

    with TImageSVGList(GetComponent) do
        try
        Result := editor.Execute(List, Name);
        if Result and Rendering then List.ForceRendering;
        finally
        editor.Free;
        end;
  end;

function TImageSVGListEditor.DoClear: Boolean;
  begin
    with TImageSVGList(GetComponent) do
      begin
      Result := List.Count > 0;
      List.Clear;
      if Rendering then List.ForceRendering;
      end;
  end;

end.
