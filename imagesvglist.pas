{ ImageSVGList
  ------------------------------------------------------------------------------
  A FreePascal package that implements TImageSVGList component - a list of
  SVG images instead of regular bitmaps like TImageList
  (с) Riva, 2023.10.07
  https://riva-lab.gitlab.io 
  https://gitlab.com/riva-lab
  ------------------------------------------------------------------------------

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit ImageSVGList;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LCLClasses, Controls, SVGList;

type

  { TImageSVGList
    -------------
    Non-visual design-time component-wrapper for TSVGList.
    It is a list of SVG vector images
    instead of a regular bitmaps like TImageList.
    It generates raster images into standard TImageList.
    Usually used for creating multi-PPI GUI.
    Note. Published properties have documentation FPDoc. }

  TImageSVGList = class(TLCLComponent)
  private
    FSVGList:      TSVGList;
    FFilename:     String;
    FResourceName: String;
    FLoaded:       Boolean;
    FRenderingTmp: Boolean;
    FOnRenderDone: TNotifyEvent;
    FOnChange:     TNotifyEvent;

    function GetImagesActive: TImageList;
    function GetImagesDisabled: TImageList;
    function GetDisabledBlend: TDisabledBlend;
    function GetDisabledLevel: Byte;
    function GetRenderSize: Integer;
    function GetRendering: Boolean;

    procedure SetList(AValue: TSVGList);
    procedure SetFilename(AValue: String);
    procedure SetResourceName(AValue: String);
    procedure SetImagesActive(AValue: TImageList);
    procedure SetImagesDisabled(AValue: TImageList);
    procedure SetDisabledBlend(AValue: TDisabledBlend);
    procedure SetDisabledLevel(AValue: Byte);
    procedure SetRenderSize(AValue: Integer);
    procedure SetRendering(AValue: Boolean);

    procedure DoOnChange(Sender: TObject);
    procedure DoOnRenderDone(Sender: TObject);

  protected
    procedure Loaded; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

  published
    property Rendering: Boolean read GetRendering write SetRendering default False;
    property RenderSize: Integer read GetRenderSize write SetRenderSize default 16;
    property DisabledBlend: TDisabledBlend read GetDisabledBlend write SetDisabledBlend default dbCorrectedLightness;
    property DisabledLevel: Byte read GetDisabledLevel write SetDisabledLevel default 192;
    property ImagesActive: TImageList read GetImagesActive write SetImagesActive;
    property ImagesDisabled: TImageList read GetImagesDisabled write SetImagesDisabled;

    property List: TSVGList read FSVGList write SetList;
    property LoadRes: String read FResourceName write SetResourceName;
    property LoadFile: String read FFilename write SetFilename;

    property OnRenderDone: TNotifyEvent read FOnRenderDone write FOnRenderDone;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;


procedure Register;


implementation

procedure Register;
  begin
    RegisterComponents('Common Controls', [TImageSVGList]);
  end;


{ TImageSVGList }

function TImageSVGList.GetImagesActive: TImageList;
  begin
    Result := FSVGList.ImagesActive;
  end;

function TImageSVGList.GetImagesDisabled: TImageList;
  begin
    Result := FSVGList.ImagesDisabled;
  end;

function TImageSVGList.GetDisabledBlend: TDisabledBlend;
  begin
    Result := FSVGList.DisabledBlend;
  end;

function TImageSVGList.GetDisabledLevel: Byte;
  begin
    Result := FSVGList.DisabledLevel;
  end;

function TImageSVGList.GetRenderSize: Integer;
  begin
    Result := FSVGList.RenderSize;
  end;

function TImageSVGList.GetRendering: Boolean;
  begin
    Result := FSVGList.Rendering;
  end;

procedure TImageSVGList.SetList(AValue: TSVGList);
  begin
    if AValue = nil then Exit;
    FSVGList.Assign(AValue);
  end;

procedure TImageSVGList.SetFilename(AValue: String);
  begin
    if FFilename = AValue then Exit;
    FFilename := AValue;

    if FFilename = '' then Exit;

    if LowerCase(ExtractFileExt(FFilename)) <> TSVGList.ExtensionSVGList then
      FFilename += TSVGList.ExtensionSVGList;

    FSVGList.LoadFromFile(FFilename);
  end;

procedure TImageSVGList.SetResourceName(AValue: String);
  begin
    if FResourceName = AValue then Exit;
    FResourceName := AValue;

    // external file has highest priority,
    // so we don't load if filename already set
    if FFilename <> '' then Exit;

    FSVGList.LoadFromResource(FResourceName);
  end;

procedure TImageSVGList.SetImagesActive(AValue: TImageList);
  begin
    FSVGList.ImagesActive := AValue;
  end;

procedure TImageSVGList.SetImagesDisabled(AValue: TImageList);
  begin
    FSVGList.ImagesDisabled := AValue;
  end;

procedure TImageSVGList.SetDisabledBlend(AValue: TDisabledBlend);
  begin
    FSVGList.DisabledBlend := AValue;
  end;

procedure TImageSVGList.SetDisabledLevel(AValue: Byte);
  begin
    FSVGList.DisabledLevel := AValue;
  end;

procedure TImageSVGList.SetRenderSize(AValue: Integer);
  begin
    FSVGList.RenderSize := AValue;
  end;

procedure TImageSVGList.SetRendering(AValue: Boolean);
  begin
    if not FLoaded then
      FRenderingTmp := AValue;

    FSVGList.Rendering := AValue;
  end;

procedure TImageSVGList.DoOnChange(Sender: TObject);
  begin
    if Assigned(FOnChange) then FOnChange(Self);
  end;

procedure TImageSVGList.DoOnRenderDone(Sender: TObject);
  begin
    if Assigned(FOnRenderDone) then FOnRenderDone(Self);
  end;

procedure TImageSVGList.Loaded;
  begin
    inherited Loaded;
    FLoaded   := True;
    Rendering := FRenderingTmp;
  end;

constructor TImageSVGList.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);

    FSVGList              := TSVGList.Create(nil, nil);
    FSVGList.OnChange     := @DoOnChange;
    FSVGList.OnRenderDone := @DoOnRenderDone;

    FFilename     := '';
    FResourceName := '';
    FLoaded       := False;
    FRenderingTmp := False;
  end;

destructor TImageSVGList.Destroy;
  begin
    FSVGList.Free;

    inherited Destroy;
  end;

end.
