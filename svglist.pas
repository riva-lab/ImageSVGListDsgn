﻿{ SVGList.pas                                      |  (c) 2023 Riva   |  v1.5  |
  ------------------------------------------------------------------------------
  A FreePascal module that implements a list of SVG vector images
  instead of a regular bitmaps. It can generate raster images to target
  standard TImageList. Usually used to create multi-PPI GUI.
  ------------------------------------------------------------------------------

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit SVGList;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, FileUtil, Controls, LCLType, Graphics,
  BGRASVGImageList, BGRABitmapTypes, BGRABitmap;


type

  { TDisabledBlend
    --------------
    Blending mode used to create disabled images }

  TDisabledBlend = (dbLighten, dbDarken, dbLinearBlend, dbLinearLightness,
    dbCorrectedLightness, dbHardLight, dbGlow);


  { TSVGList
    --------
    A class that implements a list of SVG icons.
    Allows you to include SVG icons in your project.
    So this allows you to change the icon resolution on the fly.
    Features:
    - loading from and saving to a simple text file .svglist
    - loading from resources
    - loading from stream
    - rendering icons to a standard TImageList
    - rendering of disabled icons with user-set luminosity level
    - operations with a list of icons
    --------
    Класс для работы с SVG-иконками.
    Предназначен для внедрения в проект иконок в векторном формате SVG.
    Это дает возможность изменять размер иконок в приложении на лету.
    Возможности:
    - загрузка и сохранение в файл - компактный список .svglist
    - загрузка из ресурсов
    - загрузка из потока
    - рендеринг иконок в стандартный TImageList
    - рендеринг обесцвеченных "отключенных" иконок (яркость настраивается)
    - операции со списком иконок
    --------
    (с) Riva, 2023.09.30
    https://riva-lab.gitlab.io
    https://gitlab.com/riva-lab
    --------
    2023.10.07   riva     v1.1 fix some bugs
    2023.10.09   riva     v1.2 add proc Assign()
                               add proc SaveSVG(filename,i)
                               add proc Exchange(i1,i2)  
                               add func Add(filename)
                               corrected proc Delete()    
                               corrected func Add(name,svg)
                               fixed bug in SetSVGName()
                               check max RenderSize  
    2023.10.11   riva     v1.3 add func GetBGRABitmapDisabled()
                               correct func GetBGRABitmap()
                               add event OnChange
                               add event OnRenderDone 
    2023.10.12   riva     v1.4 override proc DefineProperties()
                               add proc ReadSVGNames()
                               add proc WriteSVGNames()
                               correct func Add(name,svg)
    2023.10.19   riva     v1.5 add DisableBlend property
  }

  TSVGList = class(TStringList)
  const
    Version          = '1.5';      // must be equal to this file version
    ExtensionSVG     = '.svg';     // must be lowercase
    ExtensionSVGList = '.svglist'; // must be lowercase
    RenderSizeMax    = 1024;       // max height and width of rendered images

  private
    FSVGName:          TStringList;
    FBGRASVGImageList: TBGRASVGImageList;
    FImagesActive:     TImageList;
    FImagesDisabled:   TImageList;
    FOnRenderDone:     TNotifyEvent;
    FOnChange:         TNotifyEvent;

    FPath:          String;
    FRenderSize:    Integer;
    FRendering:     Boolean;
    FDisabledBlend: TDisabledBlend;
    FDisabledLevel: Byte;

    function GetSVGName(Index: Integer): String;
    procedure SetSVGName(Index: Integer; AValue: String);
    procedure SetImagesActive(AValue: TImageList);
    procedure SetImagesDisabled(AValue: TImageList);
    procedure SetDisabledBlend(AValue: TDisabledBlend);
    procedure SetDisabledLevel(AValue: Byte);
    procedure SetRenderSize(AValue: Integer);
    procedure SetRendering(AValue: Boolean);

    procedure SVGNamesCheck;
    procedure TryRenderSVGToLists;
    procedure RenderSVGToLists;
    procedure UpdateBGRASVGImageList;

  protected
    // add custom properties to streaming data
    procedure DefineProperties(Filer: TFiler); override;
    procedure ReadSVGNames(Reader: TReader);   // reader for streaming
    procedure WriteSVGNames(Writer: TWriter);  // writer for streaming

  public
    constructor Create(AImageListActive: TImageList = nil; AImageListDisabled: TImageList = nil);
    destructor Destroy; override;

    // copy all fields except classes TBGRASVGImageList, TImageList and events
    procedure Assign(Source: TPersistent); override;

    procedure LoadFromString(const AStr: String);
    procedure LoadFromFile(const AFilename: String); override;
    procedure LoadFromStream(Stream: TStream); override;
    procedure LoadFromResource(const ResName: String; ResType: PChar = RT_RCDATA);
    procedure SaveToFile(const AFilename: String); override;
    procedure SaveSVG(AIndex: Integer; const AFilename: String);

    procedure Clear; override;
    procedure Delete(Index: Integer); override;
    function Add(const AName, ASVG: String): Integer;
    function Add(const AFilename: String): Integer;
    procedure Exchange(Index1, Index2: Integer); override;

    procedure ForceRendering;
    function GetBGRABitmap(Index: Integer; ARenderSize: Integer = 0): TBGRABitmap;
    function GetBGRABitmapDisabled(Index: Integer; ARenderSize: Integer = 0): TBGRABitmap;

    // (!) use for editor only: remove from list items whose corresponding files are not available
    function DeleteUnavailableFiles: Boolean;


    // (!) use for editor only: work path where current files are located
    property Path: String read FPath write FPath;

    // name of SVG icon (based on filename)
    property SVGName[Index: Integer]: String read GetSVGName write SetSVGName;

    // container for SVG icons
    property BGRASVGImageList: TBGRASVGImageList read FBGRASVGImageList;

    // list of active regular images
    property ImagesActive: TImageList read FImagesActive write SetImagesActive;

    // list of disabled images (usually light and grayed)
    property ImagesDisabled: TImageList read FImagesDisabled write SetImagesDisabled;

    // blending mode used to create disabled images
    property DisabledBlend: TDisabledBlend read FDisabledBlend write SetDisabledBlend;

    // luminosity level for disabled icons: 0..255 = darker..lighter
    property DisabledLevel: Byte read FDisabledLevel write SetDisabledLevel;

    // size of raster images in lists
    property RenderSize: Integer read FRenderSize write SetRenderSize;

    // enable or disable rendering
    property Rendering: Boolean read FRendering write SetRendering;

    // event caused on the end of rendering process
    property OnRenderDone: TNotifyEvent read FOnRenderDone write FOnRenderDone;

    // event caused on change state
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;


function db2bo(ADB: TDisabledBlend): TBlendOperation;


implementation


function db2bo(ADB: TDisabledBlend): TBlendOperation;
  begin
    case ADB of
      dbLighten: Result            := boLighten;
      dbDarken: Result             := boDarken;
      dbLinearBlend: Result        := boLinearBlend;
      dbLinearLightness: Result    := boLinearLightness;
      dbCorrectedLightness: Result := boCorrectedLightness;
      dbHardLight: Result          := boHardLight;
      dbGlow: Result               := boGlow;
      end;
  end;


{ TSVGList }

function TSVGList.GetSVGName(Index: Integer): String;
  begin
    Result := '';
    if FSVGName.Count = 0 then Exit;
    if Index in [0..FSVGName.Count - 1] then
      Result := FSVGName[Index];
  end;

procedure TSVGList.SetSVGName(Index: Integer; AValue: String);
  begin
    if Index >= FSVGName.Count then
      SVGNamesCheck;

    if Index in [0..Count - 1] then
      begin
      FSVGName[Index] := AValue;
      if Assigned(FOnChange) then FOnChange(Self);
      end;
  end;

procedure TSVGList.SetImagesActive(AValue: TImageList);
  begin
    if FImagesActive = AValue then Exit;
    FImagesActive := AValue;
    TryRenderSVGToLists;
  end;

procedure TSVGList.SetImagesDisabled(AValue: TImageList);
  begin
    if FImagesDisabled = AValue then Exit;
    FImagesDisabled := AValue;
    TryRenderSVGToLists;
  end;

procedure TSVGList.SetDisabledBlend(AValue: TDisabledBlend);
  begin
    if FDisabledBlend = AValue then Exit;
    FDisabledBlend := AValue;

    if Assigned(FOnChange) then FOnChange(Self);
    TryRenderSVGToLists;
  end;

procedure TSVGList.SetDisabledLevel(AValue: Byte);
  begin
    if FDisabledLevel = AValue then Exit;
    FDisabledLevel := AValue;

    if Assigned(FOnChange) then FOnChange(Self);
    TryRenderSVGToLists;
  end;

procedure TSVGList.SetRenderSize(AValue: Integer);
  begin
    if AValue > RenderSizeMax then AValue := RenderSizeMax;
    if FRenderSize = AValue then Exit;
    FRenderSize := AValue;

    if Assigned(FOnChange) then FOnChange(Self);
    TryRenderSVGToLists;
  end;

procedure TSVGList.SetRendering(AValue: Boolean);
  begin
    if FRendering = AValue then Exit;
    FRendering := AValue;

    if Assigned(FOnChange) then FOnChange(Self);
    TryRenderSVGToLists;
  end;

procedure TSVGList.SVGNamesCheck;
  begin
    while FSVGName.Count < Count do
      FSVGName.Add('');
  end;

procedure TSVGList.TryRenderSVGToLists;
  begin
    if FBGRASVGImageList.Count = 0 then
      UpdateBGRASVGImageList
    else
      RenderSVGToLists;
  end;

procedure TSVGList.RenderSVGToLists;
  var
    i: Integer;
    c: TBGRAPixel;
  begin
    if (FImagesActive = nil) and (FImagesDisabled = nil) then Exit;

    if FRendering and (FRenderSize >= 0) then
      begin
      if FImagesActive <> nil then
        begin
        FImagesActive.Width  := FRenderSize;
        FImagesActive.Height := FRenderSize;
        FImagesActive.Clear;
        end;

      if FImagesDisabled <> nil then
        begin
        FImagesDisabled.Width  := FRenderSize;
        FImagesDisabled.Height := FRenderSize;
        FImagesDisabled.Clear;
        end;

      if FBGRASVGImageList.Count = 0 then Exit;

      FBGRASVGImageList.Width  := FRenderSize;
      FBGRASVGImageList.Height := FRenderSize;

      c := ColorToBGRA(FDisabledLevel * $10101);

      for i := 0 to FBGRASVGImageList.Count - 1 do
        with FBGRASVGImageList.GetBGRABitmap(i, FRenderSize, FRenderSize) do
            try
            // add regular icons
            if FImagesActive <> nil then
              FImagesActive.Add(Bitmap, nil);

            // add light disabled icons
            if FImagesDisabled <> nil then
              begin
              Blend(c, db2bo(FDisabledBlend), True);
              FImagesDisabled.Add(Bitmap, nil);
              end;
            finally
            Free;
            end;

      if Assigned(FOnRenderDone) then FOnRenderDone(Self);
      end;
  end;

procedure TSVGList.UpdateBGRASVGImageList;
  var
    i: Integer;
  begin
    if Count > 0 then
      for i := 0 to Count - 1 do
        if i < FBGRASVGImageList.Count then
          FBGRASVGImageList.Replace(i, Self.Strings[i])
        else
          FBGRASVGImageList.Add(Self.Strings[i]);

    // remove unused svg-s
    while Count < FBGRASVGImageList.Count do
      FBGRASVGImageList.Remove(FBGRASVGImageList.Count - 1);

    RenderSVGToLists;
  end;

constructor TSVGList.Create(AImageListActive: TImageList;
  AImageListDisabled: TImageList);
  begin
    inherited Create;
    FSVGName          := TStringList.Create;
    FBGRASVGImageList := TBGRASVGImageList.Create(nil);
    FBGRASVGImageList.TargetRasterImageList := nil;

    FImagesActive   := AImageListActive;
    FImagesDisabled := AImageListDisabled;
    FDisabledBlend  := dbCorrectedLightness;
    FDisabledLevel  := 192;
    FRenderSize     := 16;
    FRendering      := False;
    FPath           := '';
    FOnChange       := nil;
    FOnRenderDone   := nil;
  end;

destructor TSVGList.Destroy;
  begin
    FBGRASVGImageList.Free;
    FSVGName.Free;
    inherited Destroy;
  end;

procedure TSVGList.Assign(Source: TPersistent);
  var
    buOnChange: TNotifyEvent;
  begin
    if Source = nil then Exit;
    if Source.ClassType <> TSVGList then Exit;

    // disable OnChange event to prevent unexpected call
    buOnChange := FOnChange;
    FOnChange  := nil;

    inherited Assign(Source);
    FSVGName.Assign(TSVGList(Source).FSVGName);

    FPath          := TSVGList(Source).FPath;
    FRenderSize    := TSVGList(Source).FRenderSize;
    FRendering     := TSVGList(Source).FRendering;
    FDisabledBlend := TSVGList(Source).FDisabledBlend;
    FDisabledLevel := TSVGList(Source).FDisabledLevel;

    // restore OnChange event
    FOnChange := buOnChange;

    if Assigned(FOnChange) then FOnChange(Self);
  end;

procedure TSVGList.LoadFromString(const AStr: String);
  var
    i: Integer;
  begin
    if AStr = '' then Exit;
    Clear;

    Self.Text := AStr;

    if Count > 0 then
      begin

      // read names to other list
      for i := 0 to Count - 1 do
        if Self[i].StartsWith('<') then
          FSVGName.Add(Self[i - 1]);

      // remove names from loaded list
      for i := (Count div 2) - 1 downto 0 do
        inherited Delete(i * 2);

      if Assigned(FOnChange) then FOnChange(Self);
      UpdateBGRASVGImageList;
      end;
  end;

procedure TSVGList.LoadFromFile(const AFilename: String);
  begin
    if not FileExistsUTF8(AFilename) then Exit;
    FPath := ExtractFilePath(AFilename);
    LoadFromString(ReadFileToString(AFilename));
  end;

procedure TSVGList.LoadFromStream(Stream: TStream);
  var
    strStream: TStringStream;
  begin
      try
      strStream := TStringStream.Create;
      strStream.LoadFromStream(Stream);
      LoadFromString(strStream.DataString);
      finally
      strStream.Free;
      end;
  end;

procedure TSVGList.LoadFromResource(const ResName: String; ResType: PChar);
  var
    resStream: TResourceStream;
  begin
    if FindResource(HInstance, ResName, ResType) = 0 then Exit;

      try
      resStream := TResourceStream.Create(HInstance, ResName, ResType);
      LoadFromStream(resStream);
      finally
      resStream.Free;
      end;
  end;

procedure TSVGList.SaveToFile(const AFilename: String);
  var
    i:   Integer;
    svg: String;
  begin
    if AFilename = '' then Exit;
    FPath := ExtractFilePath(AFilename);

    with TStringList.Create do
        try
        for i := 0 to Self.Count - 1 do
          begin
          // some optimizations
          svg := Self[i].Replace(#13, ' ').Replace(#10, ' ');
          svg := svg.Replace('> ', '>');
          svg := svg.Replace('  ', ' ').Replace('  ', ' ');
          svg := svg.Replace('  ', ' ').Replace('  ', ' ');

          Add(SVGName[i]);  // add name
          Add(svg);         // add svg
          end;

        SaveToFile(AFilename);
        finally
        Free;
        end;
  end;

procedure TSVGList.SaveSVG(AIndex: Integer; const AFilename: String);
  begin
    if AIndex < 0 then Exit;
    if AIndex >= Count then Exit;
    if AFilename = '' then Exit;

    with TStringList.Create do
        try
        Text := Self[AIndex];
        SaveToFile(AFilename);
        finally
        Free;
        end;
  end;

procedure TSVGList.Clear;
  begin
    inherited Clear;
    FSVGName.Clear;

    if Assigned(FOnChange) then FOnChange(Self);
    UpdateBGRASVGImageList;
  end;

procedure TSVGList.Delete(Index: Integer);
  begin
    if Index in [0..Count - 1] then
      begin
      SVGNamesCheck;
      inherited Delete(Index);
      FSVGName.Delete(Index);

      if Assigned(FOnChange) then FOnChange(Self);
      UpdateBGRASVGImageList;
      end;
  end;

function TSVGList.Add(const AName, ASVG: String): Integer;
  begin
    Result := -1;
    if (AName = '') or (FSVGName.IndexOf(AName) < 0) then
      begin
      Result := inherited Add(ASVG.Replace(#13, ' ').Replace(#10, ' '));

      if Result < 0 then Exit;
      SVGName[Result] := AName;

      UpdateBGRASVGImageList;
      end;
  end;

function TSVGList.Add(const AFilename: String): Integer;
  begin
    Result := -1;

    if FileExistsUTF8(AFilename) then
      Result := Add(
        ExtractFileNameOnly(AFilename),
        ReadFileToString(AFilename));
  end;

procedure TSVGList.Exchange(Index1, Index2: Integer);
  begin
    if not (Index1 in [0..Count - 1]) then Exit;
    if not (Index2 in [0..Count - 1]) then Exit;

    SVGNamesCheck;
    inherited Exchange(Index1, Index2);
    FSVGName.Exchange(Index1, Index2);

    if Assigned(FOnChange) then FOnChange(Self);
    UpdateBGRASVGImageList;
  end;

procedure TSVGList.ForceRendering;
  var
    tmp: Boolean;
  begin
    tmp        := FRendering;
    FRendering := True;
    TryRenderSVGToLists;
    FRendering := tmp;
  end;

function TSVGList.GetBGRABitmap(Index: Integer; ARenderSize: Integer): TBGRABitmap;
  begin
    if ARenderSize = 0 then ARenderSize := FRenderSize;
    if ARenderSize = 0 then Exit(nil);
    if Index < 0 then Exit(nil);
    if Index > FBGRASVGImageList.Count then Exit(nil);
    Result := FBGRASVGImageList.GetBGRABitmap(Index, ARenderSize, ARenderSize);
  end;

function TSVGList.GetBGRABitmapDisabled(Index: Integer; ARenderSize: Integer): TBGRABitmap;
  begin
    Result := GetBGRABitmap(Index, ARenderSize);
    if Result <> nil then
      Result.Blend(ColorToBGRA(FDisabledLevel * $10101), db2bo(FDisabledBlend), True);
  end;

procedure TSVGList.DefineProperties(Filer: TFiler);
  begin
    inherited DefineProperties(Filer);
    Filer.DefineProperty('SVGNames', @ReadSVGNames, @WriteSVGNames, Count > 0);
  end;

procedure TSVGList.ReadSVGNames(Reader: TReader);
  var
    i: Integer = 0;

  function Next(var Index: Integer): Integer;
    begin
      Result := Index;
      Index  += 1;
    end;

  begin
    Reader.ReadListBegin;
    while not Reader.EndOfList do
      SVGName[Next(i)] := Reader.ReadString;
    Reader.ReadListEnd;
  end;

procedure TSVGList.WriteSVGNames(Writer: TWriter);
  var
    i: Integer;
  begin
    if Count > 0 then
      begin
      Writer.WriteListBegin;
      for i := 0 to Count - 1 do
        Writer.WriteString(SVGName[i]);
      Writer.WriteListEnd;
      end;
  end;

function TSVGList.DeleteUnavailableFiles: Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if FPath = '' then Exit;

    for i := Count - 1 downto 0 do
      if not FileExistsUTF8(FPath + SVGName[i] + '.svg') then
        begin
        Delete(i);
        Result := True;
        end;
  end;

end.
