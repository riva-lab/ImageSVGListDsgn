{ ImageSVGListExample                                 
  ------------------------------------------------------------------------------
  Lazarus example project to show how to use TImageSVGList component
  ------------------------------------------------------------------------------
  MIT License
  Copyright (c) 2023 Riva
}

unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Dialogs, ComCtrls, Spin,
  StdCtrls, ImageSVGList, SVGList, TypInfo;

type

  { TForm1 }

  TForm1 = class(TForm)
    cbEnabled:     TCheckBox;
    cbRender:      TCheckBox;
    cbBlend:       TComboBox;
    imActive:      TImageList;
    ImageSVGList1: TImageSVGList;
    imDisabled:    TImageList;
    Label1:        TLabel;
    Label2:        TLabel;
    Label3:        TLabel;
    seDis:         TSpinEdit;
    seRes:         TSpinEdit;
    ToolBar1:      TToolBar;
    ToolButton1:   TToolButton;
    ToolButton2:   TToolButton;
    ToolButton3:   TToolButton;
    ToolButton4:   TToolButton;

    procedure FormCreate(Sender: TObject);
    procedure seResChange(Sender: TObject);
    procedure seDisChange(Sender: TObject);
    procedure cbEnabledChange(Sender: TObject);
    procedure cbRenderChange(Sender: TObject);
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
  var
    item: TDisabledBlend;
  begin
    cbBlend.Clear;
    for item in TDisabledBlend do
      cbBlend.Items.Add(GetEnumName(TypeInfo(TDisabledBlend), Ord(item)));

    cbBlend.ItemIndex := Ord(ImageSVGList1.DisabledBlend);
    cbBlend.OnChange  := @seDisChange;

    seResChange(Sender);
    seDisChange(Sender);
    cbEnabledChange(Sender);
    cbRenderChange(Sender);
  end;

procedure TForm1.seResChange(Sender: TObject);
  begin
    ImageSVGList1.RenderSize := seRes.Value;
    ToolBar1.ButtonHeight    := seRes.Value + 16;
    ToolBar1.ButtonWidth     := seRes.Value + 16;
  end;

procedure TForm1.seDisChange(Sender: TObject);
  begin
    ImageSVGList1.DisabledBlend := TDisabledBlend(cbBlend.ItemIndex);
    ImageSVGList1.DisabledLevel := seDis.Value;
  end;

procedure TForm1.cbEnabledChange(Sender: TObject);
  var
    i: Integer;
  begin
    cbBlend.Enabled := not cbEnabled.Checked;
    seDis.Enabled   := not cbEnabled.Checked;

    for i := 0 to ToolBar1.ButtonCount - 1 do
      ToolBar1.Buttons[i].Enabled := cbEnabled.Checked;
  end;

procedure TForm1.cbRenderChange(Sender: TObject);
  begin
    ImageSVGList1.Rendering := cbRender.Checked;
  end;

end.
