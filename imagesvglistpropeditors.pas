{ ImageSVGListPropEditors
  ------------------------------------------------------------------------------
  1) Editor for TSVGList type property, opens visual editor of TImageSVGList
  2) Dialog for property TImageSVGList.LoadFile - .svglist file selection
  (с) Riva, 2023.10.11
  https://riva-lab.gitlab.io
  https://gitlab.com/riva-lab
  ------------------------------------------------------------------------------

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit ImageSVGListPropEditors;

{$mode objfpc}{$H+}

interface

uses
  Classes, Dialogs, PropEdits, ComponentEditors,
  SVGList, ImageSVGList, ImageSVGListEditor, ImageSVGListStrConsts;

type

  { TSVGListEditor
    --------------
    Editor for TSVGList type property.
    In fact it opens visual editor of TImageSVGList }

  TSVGListEditor = class(TPropertyEditor)
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
    function GetValue: ansistring; override;
  end;

  { TSVGListFileNameEditor
    ----------------------
    Dialog for property TImageSVGList.LoadFile - .svglist file selection }

  TSVGListFileNameEditor = class(TFileNamePropertyEditor)
  public
    function GetFilter: String; override;
    function GetDialogOptions: TOpenOptions; override;
    function GetDialogTitle: String; override;
  end;

procedure Register;


implementation

procedure Register;
  begin
    // register editor for TSVGList type property
    RegisterPropertyEditor(TypeInfo(TSVGList), nil, '', TSVGListEditor);

    // register dialog for property TImageSVGList.LoadFile
    RegisterPropertyEditor(TypeInfo(AnsiString), TImageSVGList, 'LoadFile', TSVGListFileNameEditor);
  end;


{ TSVGListEditor }

procedure TSVGListEditor.Edit;
  var
    ed: TImageSVGListEditor;
  begin
    ed := GetComponentEditor(TComponent(GetComponent(0)), nil) as TImageSVGListEditor;
    if Assigned(ed) and ed.DoExecute then Modified;
  end;

function TSVGListEditor.GetAttributes: TPropertyAttributes;
  begin
    Result := [paMultiSelect, paDialog, paReadOnly];
  end;

function TSVGListEditor.GetValue: ansistring;
  begin
    Result := '(' + TSVGList.ClassName + ')';
  end;


{ TSVGListFileNameEditor }

function TSVGListFileNameEditor.GetFilter: String;
  begin
    Result := rsEdExtSVGList + '|*' + TSVGList.ExtensionSVGList;
    Result += '|' + rsPropEdExtAll + '|*.*';
  end;

function TSVGListFileNameEditor.GetDialogOptions: TOpenOptions;
  begin
    Result := inherited GetDialogOptions + [ofFileMustExist, ofPathMustExist];
  end;

function TSVGListFileNameEditor.GetDialogTitle: String;
  begin
    Result := rsPropEdDialog;
  end;

end.
