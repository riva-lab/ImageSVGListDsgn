{ ImageSVGListStrConsts
  ------------------------------------------------------------------------------
  TImageSVGList component and linked units localization
  (с) Riva, 2023.10.11
  https://riva-lab.gitlab.io
  https://gitlab.com/riva-lab
  ------------------------------------------------------------------------------

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit ImageSVGListStrConsts;

{$mode objfpc}{$H+}

interface

resourcestring

  // ImageSVGListEditor
  rsMenuEdit      = 'I&mageSVGList Editor ...';
  rsMenuClear     = '&Clear ImageSVGList';

  // ImageSVGListEditorForm                 
  rsEdCaption     = 'ImageSVGList Editor - %s';
  rsEdTryGetHelp  = 'Try to get help on %s';
  rsEdExtSVG      = 'SVG image';
  rsEdExtSVGList  = 'SVGList file - list of SVG images';
  rsEdActive      = 'Active';
  rsEdDisabled    = 'Disabled';
  rsEdFilename    = 'Filename';
  rsEdSVG         = 'List of SVG images: %d';
  rsEdSelected    = 'Selected image';
  rsEdSelIndex    = 'Index: %d';
  rsEdDragNDrop   = 'or just drag-n-drop your SVG files or %s file';
  rsEdSlider      = 'move slider to change background (only for preview)';
  rsEdAddList     = 'Add from list ...';
  rsEdAddListHint = 'Add images from %s file';
  rsEdAddSVG      = 'Add SVG image ...';
  rsEdMoveUp      = 'Move up';
  rsEdMoveDown    = 'Move down';
  rsEdDelete      = 'Delete';
  rsEdClear       = 'Clear';
  rsEdLoad        = 'Load list ...';
  rsEdLoadHint    = 'Load from %s file';
  rsEdSave        = 'Save list ...';
  rsEdSaveHint    = 'Save as %s file';
  rsEdSaveSVG     = 'Save SVG ...';
  rsEdSaveSVGHint = 'Save selected item as SVG image';
  rsEdDlgOpenList = 'Open SVG list';
  rsEdDlgOpenSVG  = 'Open SVG file';
  rsEdDlgSaveList = 'Save SVG List';
  rsEdDlgSaveSVG  = 'Save SVG file';

  // TSVGListFileNameEditor
  rsPropEdExtAll  = 'All files';
  rsPropEdDialog  = 'Select SVGList file';

implementation

end.
