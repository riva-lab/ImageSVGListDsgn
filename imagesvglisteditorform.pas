{ ImageSVGListEditorForm
  ------------------------------------------------------------------------------
  Form of visual editor for TImageSVGList component
  (с) Riva, 2023.10.10
  https://riva-lab.gitlab.io
  https://gitlab.com/riva-lab
  ------------------------------------------------------------------------------

  MIT License
  ------------------------------------------------------------------------------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
  -----------------------------------------------------------------------------}

unit ImageSVGListEditorForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Grids,
  ButtonPanel, ComCtrls, ExtCtrls, ActnList, Spin, LCLType, IDEWindowIntf,
  TypInfo, Math, BGRABitmap, SVGList, ImageSVGListStrConsts;

const
  constPkgURL  = 'https://gitlab.com/riva-lab/ImageSVGListDsgn';
  constMaxRowH = 64;

type

  { TfmImageSVGListEditor
    ---------------------
    Visual editor for TImageSVGList component.
    In fact it edits only TSVGList property of component }

  TfmImageSVGListEditor = class(TForm)
    acAddList:        TAction;
    acAddSVG:         TAction;
    acClear:          TAction;
    acDelete:         TAction;
    acLoadList:       TAction;
    acMoveDn:         TAction;
    acMoveUp:         TAction;
    acSaveList:       TAction;
    acSaveSVG:        TAction;
    alMain:           TActionList;
    Bevel1:           TBevel;
    Bevel2:           TBevel;
    Button1:          TButton;
    Button10:         TButton;
    Button2:          TButton;
    Button4:          TButton;
    Button5:          TButton;
    Button6:          TButton;
    Button7:          TButton;
    Button8:          TButton;
    Button9:          TButton;
    ButtonPanel1:     TButtonPanel;
    dgList:           TDrawGrid;
    dlgSVGListOpen:   TOpenDialog;
    dlgSVGListSave:   TSaveDialog;
    gbList:           TGroupBox;
    gbSVG:            TGroupBox;
    imSvgA:           TImageList;
    imSvgD:           TImageList;
    Label2:           TLabel;
    Label3:           TLabel;
    lbDragDrop:       TLabel;
    lbSlider:         TLabel;
    Panel1:           TPanel;
    Panel3:           TPanel;
    pBack:            TPanel;
    pbSvgA:           TPaintBox;
    pbSvgD:           TPaintBox;
    ScrollBox1:       TScrollBox;
    seDisLevel:       TSpinEdit;
    seRenSize:        TSpinEdit;
    trBack:           TTrackBar;
    tmrAccessibility: TTimer;
    lbItem:           TLabel;
    cbBlend:          TComboBox;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure HelpButtonClick(Sender: TObject);
    procedure actionExecute(Sender: TObject);
    procedure trBackMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure trBackChange(Sender: TObject);
    procedure seRenSizeChange(Sender: TObject);
    procedure seDisLevelChange(Sender: TObject);
    procedure pbSvgAPaint(Sender: TObject);
    procedure pbSvgDPaint(Sender: TObject);
    procedure dgListSelectCell(Sender: TObject; aCol, aRow: Integer; var CanSelect: Boolean);
    procedure dgListDrawCell(Sender: TObject; aCol, aRow: Integer; aRect: TRect; aState: TGridDrawState);
    procedure dgListKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tmrAccessibilityTimer(Sender: TObject);

  private
    FSVGList:      TSVGList;
    FConstWidth:   Integer;
    FDefRowHeight: Integer;
    FDefNameWidth: Integer;

    procedure Localize;
    procedure DrawGridSetup;
    procedure PaintBoxUpdate(APaintBox: TPaintBox);

  public
    // show modal form and return true if editing was applied
    function Execute(ASVGList: TSVGList; ACompName: String): Boolean;
  end;


implementation

{$R *.lfm}

{ TfmImageSVGListEditor }

procedure TfmImageSVGListEditor.FormCreate(Sender: TObject);
  var
    item: TDisabledBlend;
  begin
    FSVGList        := TSVGList.Create(imSvgA, imSvgD);
    dgList.RowCount := dgList.FixedRows;

    cbBlend.Clear;
    for item in TDisabledBlend do
      cbBlend.Items.Add(GetEnumName(TypeInfo(TDisabledBlend), Ord(item)));

    Localize;
  end;

procedure TfmImageSVGListEditor.FormShow(Sender: TObject);
  var
    tmp: Integer;
  begin
    FSVGList.Rendering := False;
    FSVGList.Rendering := True;  // force auto-rendering

    dgList.ColWidths[0] := Canvas.GetTextWidth('0') * 5;
    FDefNameWidth       := Canvas.GetTextWidth('0') * 24;
    FDefRowHeight       := Canvas.GetTextHeight('0') + 8;
    DrawGridSetup;

    cbBlend.ItemIndex   := Ord(FSVGList.DisabledBlend);
    cbBlend.OnChange    := @seDisLevelChange;
    seDisLevel.MaxValue := FSVGList.DisabledLevel.MaxValue;
    seDisLevel.MinValue := FSVGList.DisabledLevel.MinValue;
    seDisLevel.Value    := FSVGList.DisabledLevel;
    seRenSize.MinValue  := 0;
    seRenSize.MaxValue  := FSVGList.RenderSizeMax;
    seRenSize.Value     := FSVGList.RenderSize;
    trBack.Position     := ColorToRGB(clForm) and $FF;

    dgList.Constraints.MinWidth := FConstWidth + FDefNameWidth;

    AutoSize := True;
    AutoSize := False;

    Constraints.MinWidth  := Width - FDefNameWidth;
    Constraints.MinHeight := Height;
    IDEDialogLayoutList.ApplyLayout(Self);

    tmp    := Height;
    Height := Min(tmp, round(Screen.Height * 0.7));
    tmp    := Width;
    Width  := Min(tmp, round(Screen.Width * 0.7));

    if tmp > Screen.Width - 16 then
      WindowState := wsMaximized;

    Position := poScreenCenter;
  end;

procedure TfmImageSVGListEditor.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
  begin
    IDEDialogLayoutList.SaveLayout(Self);
  end;

procedure TfmImageSVGListEditor.FormDropFiles(Sender: TObject;
  const FileNames: array of String);
  var
    s: String;
  begin
    FSVGList.Rendering := False;

    for s in FileNames do
      case LowerCase(ExtractFileExt(s)) of

        TSVGList.ExtensionSVG:
          begin
          dlgSVGListOpen.FileName := s;
          dlgSVGListOpen.Tag      := 1; // non-zero tag disables dialog showing
          acAddSVG.Execute;
          end;

        TSVGList.ExtensionSVGList:
          begin
          dlgSVGListOpen.FileName := s;
          dlgSVGListOpen.Tag      := 1; // non-zero tag disables dialog showing
          acAddList.Execute;
          end;
        end;

    FSVGList.Rendering := True;
  end;


procedure TfmImageSVGListEditor.Localize;
  begin
    dgList.Columns.Items[1].Title.Caption := rsEdActive;
    dgList.Columns.Items[2].Title.Caption := rsEdDisabled;
    dgList.Columns.Items[3].Title.Caption := rsEdFilename;

    gbSVG.Caption      := rsEdSelected;
    lbDragDrop.Caption := Format(rsEdDragNDrop, [FSVGList.ExtensionSVGList]);
    lbSlider.Caption   := rsEdSlider;
    acAddList.Caption  := rsEdAddList;
    acAddList.Hint     := Format(rsEdAddListHint, [FSVGList.ExtensionSVGList]);
    acAddSVG.Caption   := rsEdAddSVG;
    acMoveUp.Caption   := rsEdMoveUp;
    acMoveDn.Caption   := rsEdMoveDown;
    acDelete.Caption   := rsEdDelete;
    acClear.Caption    := rsEdClear;
    acLoadList.Caption := rsEdLoad;
    acSaveList.Caption := rsEdSave;
    acSaveSVG.Caption  := rsEdSaveSVG;
    acAddSVG.Hint      := rsEdAddSVG;
    acMoveUp.Hint      := rsEdMoveUp;
    acMoveDn.Hint      := rsEdMoveDown;
    acDelete.Hint      := rsEdDelete;
    acClear.Hint       := rsEdClear;
    acLoadList.Hint    := Format(rsEdLoadHint, [FSVGList.ExtensionSVGList]);
    acSaveList.Hint    := Format(rsEdSaveHint, [FSVGList.ExtensionSVGList]);
    acSaveSVG.Hint     := rsEdSaveSVGHint;
  end;

procedure TfmImageSVGListEditor.DrawGridSetup;
  var
    i: Integer;
  begin
    with dgList do
      begin
      BeginUpdate;

      RowCount         := FixedRows + FSVGList.Count;
      DefaultRowHeight := Max(FSVGList.RenderSize, FDefRowHeight) + 4;
      RowHeights[0]    := FDefRowHeight;
      ColWidths[1]     := Max(DefaultRowHeight + 32, Canvas.GetTextWidth(Columns[1].Title.Caption) + 8);
      ColWidths[2]     := Max(DefaultRowHeight + 32, Canvas.GetTextWidth(Columns[2].Title.Caption) + 8);
      FConstWidth      := 0;

      for i := 0 to ColCount - 2 do
        FConstWidth += ColWidths[i];

      EndUpdate;
      end;
  end;

function TfmImageSVGListEditor.Execute(ASVGList: TSVGList; ACompName: String): Boolean;
  var
    renderTmp: Boolean;
  begin
    FSVGList.Assign(ASVGList);
    renderTmp := ASVGList.Rendering;
    Caption   := Format(rsEdCaption, [ACompName]);

    ShowModal;
    Result := ModalResult = mrOk;

    if Result then
      begin
      ASVGList.Assign(FSVGList);
      ASVGList.RenderSize := seRenSize.Value;
      ASVGList.Rendering  := renderTmp;
      end;
  end;


procedure TfmImageSVGListEditor.HelpButtonClick(Sender: TObject);
  begin
    ShowMessageFmt(rsEdTryGetHelp, [constPkgURL]);
  end;

procedure TfmImageSVGListEditor.actionExecute(Sender: TObject);
  var
    item, i: Integer;
    tmp:     Boolean;
  begin
    item := dgList.Row - dgList.FixedRows;

    case TComponent(Sender).Name of

      'acAddSVG':
        begin
        dlgSVGListOpen.Title  := rsEdDlgOpenSVG;
        dlgSVGListOpen.Filter := rsEdExtSVG + '|*' + TSVGList.ExtensionSVG;

        // non-zero tag disables dialog showing
        if (dlgSVGListOpen.Tag <> 0) or dlgSVGListOpen.Execute then
          item := FSVGList.Add(dlgSVGListOpen.FileName);
        end;

      'acAddList':
        begin
        dlgSVGListOpen.Title  := rsEdDlgOpenList;
        dlgSVGListOpen.Filter := rsEdExtSVGList + '|*' + TSVGList.ExtensionSVGList;

        // non-zero tag disables dialog showing
        if (dlgSVGListOpen.Tag <> 0) or dlgSVGListOpen.Execute then
          with TSVGList.Create do
              try
              LoadFromFile(dlgSVGListOpen.FileName);

              if Count > 0 then
                begin
                tmp                := FSVGList.Rendering;
                FSVGList.Rendering := False; // disable temporarily

                item  := dgList.RowCount - dgList.FixedRows;
                for i := 0 to Count - 1 do
                  FSVGList.Add(SVGName[i], Strings[i]);

                FSVGList.Rendering := tmp;   // restore previous state
                end;
              finally
              Free;
              end;
        end;

      'acMoveUp':
        begin
        FSVGList.Exchange(item, item - 1);
        item -= 1;
        end;

      'acMoveDn':
        begin
        FSVGList.Exchange(item, item + 1);
        item += 1;
        end;

      'acDelete':
        begin
        FSVGList.Delete(item);
        end;

      'acClear':
        begin
        FSVGList.Clear;
        end;

      'acLoadList':
        begin
        dlgSVGListOpen.Title  := rsEdDlgOpenList;
        dlgSVGListOpen.Filter := rsEdExtSVGList + '|*' + TSVGList.ExtensionSVGList;
        if dlgSVGListOpen.Execute then
          FSVGList.LoadFromFile(dlgSVGListOpen.FileName);
        end;

      'acSaveList':
        begin
        dlgSVGListSave.Title  := rsEdDlgSaveList;
        dlgSVGListSave.Filter := rsEdExtSVGList + '|*' + TSVGList.ExtensionSVGList;
        if (FSVGList.Count > 0) and dlgSVGListSave.Execute then
          FSVGList.SaveToFile(dlgSVGListSave.FileName);
        end;

      'acSaveSVG':
        begin
        dlgSVGListSave.Title      := rsEdDlgSaveSVG;
        dlgSVGListSave.Filter     := rsEdExtSVG + '|*' + TSVGList.ExtensionSVG;
        dlgSVGListSave.InitialDir := ExtractFileDir(dlgSVGListSave.FileName);
        dlgSVGListSave.FileName   := FSVGList.SVGName[item];
        if (FSVGList.Count > 0) and dlgSVGListSave.Execute then
          FSVGList.SaveSVG(item, dlgSVGListSave.FileName);
        end;

      end;

    dlgSVGListOpen.Tag := 0; // restore showing dialog in future

    dgList.RowCount := dgList.FixedRows + FSVGList.Count; // update items count
    dgList.Row      := dgList.FixedRows + item;           // select current item
    dgList.Invalidate;
  end;

procedure TfmImageSVGListEditor.trBackMouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
  begin
    if WheelDelta = 0 then Exit;

    with trBack do
      Position := Position + PageSize * WheelDelta div abs(WheelDelta);

    Handled := True;
  end;

procedure TfmImageSVGListEditor.trBackChange(Sender: TObject);
  begin
    pBack.Color := trBack.Position * $10101;
  end;

procedure TfmImageSVGListEditor.seRenSizeChange(Sender: TObject);
  var
    p: TPaintBox;
  begin
    // render size for image lists is constrained
    // due to performance and good looking
    FSVGList.RenderSize := EnsureRange(seRenSize.Value, 1, constMaxRowH);

    for p in [pbSvgA, pbSvgD] do
      with p.Constraints, seRenSize do
        begin
        MinHeight := Value;
        MaxHeight := Value;
        MinWidth  := Value;
        MaxWidth  := Value;
        end;

    DrawGridSetup;
  end;

procedure TfmImageSVGListEditor.seDisLevelChange(Sender: TObject);
  begin
    FSVGList.DisabledLevel := seDisLevel.Value;
    FSVGList.DisabledBlend := TDisabledBlend(cbBlend.ItemIndex);
    dgList.Invalidate;
    pbSvgA.Invalidate;
    pbSvgD.Invalidate;
  end;


procedure TfmImageSVGListEditor.PaintBoxUpdate(APaintBox: TPaintBox);
  var
    b: TBGRABitmap;
    i: Integer;
  begin
    i := dgList.Row - dgList.FixedRows;

    if APaintBox = pbSvgA then
      b := FSVGList.GetBGRABitmap(i, seRenSize.Value)
    else
      b := FSVGList.GetBGRABitmapDisabled(i, seRenSize.Value);

    if b = nil then Exit;

      try
      b.Draw(APaintBox.Canvas, 0, 0, False);
      except
      APaintBox.Canvas.Clear;
      end;

    b.Free;
  end;

procedure TfmImageSVGListEditor.pbSvgAPaint(Sender: TObject);
  begin
    PaintBoxUpdate(pbSvgA);
  end;

procedure TfmImageSVGListEditor.pbSvgDPaint(Sender: TObject);
  begin
    PaintBoxUpdate(pbSvgD);
  end;

procedure TfmImageSVGListEditor.dgListSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
  begin
    pbSvgA.Invalidate;
    pbSvgD.Invalidate;
  end;

procedure TfmImageSVGListEditor.dgListDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
  var
    i: Integer;

  procedure DrawIcon(imList: TImageList; AIndex: Integer);
    begin
      imList.Draw(
        dgList.Canvas,
        aRect.CenterPoint.X - FSVGList.RenderSize div 2,
        aRect.CenterPoint.Y - FSVGList.RenderSize div 2,
        AIndex);
    end;

  begin
    with dgList do
      begin
      i := aRow - FixedRows;
      if (i < 0) or (i > FSVGList.Count - 1) then Exit;

      case aCol of
        0: Canvas.TextRect(aRect, aRect.Left, aRect.Top, '  ' + i.ToString);
        1: DrawIcon(imSvgA, i);
        2: DrawIcon(imSvgD, i);
        3: Canvas.TextRect(aRect, aRect.Left, aRect.Top, FSVGList.SVGName[i]);
        end;
      end;
  end;

procedure TfmImageSVGListEditor.dgListKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
    i: Integer;
  begin
    i := dgList.Row;

    if Shift = [ssCtrl] then
      case Key of
        VK_UP: acMoveUp.Execute;   // Ctrl + Up
        VK_DOWN: acMoveDn.Execute; // Ctrl + Dowm
        end;

    dgList.Row := i;
  end;

procedure TfmImageSVGListEditor.tmrAccessibilityTimer(Sender: TObject);
  var
    nonEmpty, onePlus: Boolean;
    index:             Integer;
  begin
    index              := dgList.Row - dgList.FixedRows;
    nonEmpty           := FSVGList.Count <> 0;
    onePlus            := FSVGList.Count > 1;
    acClear.Enabled    := nonEmpty;
    acDelete.Enabled   := nonEmpty;
    acSaveList.Enabled := nonEmpty;
    acSaveSVG.Enabled  := nonEmpty;
    acMoveUp.Enabled   := onePlus and (index > 0);
    acMoveDn.Enabled   := onePlus and (index < FSVGList.Count - 1);
    gbList.Caption     := Format(rsEdSVG, [FSVGList.Count]);
    lbItem.Caption     := Format(rsEdSelIndex, [dgList.Row - dgList.FixedRows]);
  end;

end.
